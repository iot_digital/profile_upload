//const db = require("../config/database");
const { Client } = require('pg');


const client = new Client({
    host: 'localhost',
    database: 'testdb',
    user: 'postgres',
    password: 'ubuntu',
    port: 5432,
});

client.connect(console.log("Database connected........."))
const readXlsxFile = require("read-excel-file/node");
const excel = require("exceljs");

function database(value){
  var dgs;
  const timetaken = Math.floor(Math.random()*1000)+1;
  return new Promise((resolve,reject)=>{
  setTimeout(()=>{
  var data_check = value;
  var name;
  var email;
  var phone_no;
  var ctc_currency;
  var ctcUnit;
  data_check.forEach(function(data_check) {
  name = data_check.name;
  email = data_check.email;
  phone_no = data_check.phone_no;
  ctc_currency = data_check.ctc_currency;
  ctcUnit =  data_check.ctc_type;
    })
  console.log(ctc_currency,ctcUnit)
  var ctc_check;
   if(ctc_currency == "INR" && ctcUnit == "LAKHS" || ctc_currency == "INR" && ctcUnit == "CRORES"){
      const schema = 'CREATE SCHEMA IF NOT EXISTS talent_acquisition_001;'
  
      const tableName = `CREATE TABLE IF NOT EXISTS talent_acquisition_001.candidate_summary(
          candidate_id bigserial NOT NULL,
          name varchar(100) NOT NULL,
          email_id varchar(100) NOT NULL,
          phone_number varchar(100) NOT NULL,
          created_by varchar(100) NOT NULL,
          modified_by varchar(100) NULL,
          modified_date timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          created_date timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          candidates_data jsonb NULL,
          CONSTRAINT candidate_summary_pkey15 PRIMARY KEY (candidate_id)
          );`;
      //    console.log("*******************************")
      var data = value;
      var finalData;
      data.forEach(function(data) {
          var name = data.name;
          var email = data.email;
          var phone_no = data.phone_no;
          var created_by = data.name;
          var modified_by = data.name;
          var candidates_data = {
              "ctc":  {
                 "value" : data.ctc,     
                 "ctcUnit": data.ctc_type, 
                  "ctcCurrency" : data.ctc_currency,
               },
              "candidateExperience": data.experience,
              "company ": {
                "name": data.company},
            "location" : {
               "city": data.location
                     },
            "linkedIn": data.linkedIN
            }
          
          finalData = "'"+name+"', '"+email+"', '"+phone_no+"', '"+created_by+"', '"+modified_by+"', '"+JSON.stringify(candidates_data)+"'";
          finalData = finalData+"";
          //console.log(finalData);
      });
  
      const insertData = `INSERT INTO talent_acquisition_001.candidate_summary(name,email_id, phone_number, created_by, modified_by, candidates_data) VALUES(`+finalData+`);`; 
  
  
      client.query(schema, (err, res) => {
          if (err) {
              console.error(err);
              return;
          }
          console.log('Schema created successful');
          //client.end();
      });
      
      client.query(tableName, (err, res) => {
          if (err) {
              console.error(err);
              return;
          }
          console.log('Table created successful');
          //client.end();
      }); 
      
      client.query(insertData, (err, res) => {
          if (err) {
              console.error(err);
              return;
          }
          console.log('Data inserted successful');
          //client.end();
      });
      
      
      const readData = `
      SELECT * FROM talent_acquisition_001.candidate_summary;
      `;
      
      
      
      client.query(readData, (err, res) => {
          if (err) {
              console.error(err);
              return;
          }
          for (let row of res.rows) {
              dgs = row;
              console.log(dgs)
          
           } 
           resolve (dgs);
        });
      
  }
  else if((ctc_currency == "USD" && (ctcUnit == "THOUSANDS" || ctcUnit == "MILLIONS")) || (ctc_currency == "EUR" && (ctcUnit == "Thousands" || ctcUnit == "Millions"))){
    data(data_check,file)
  // console.log(suc);
     resolve ("currency in US Dollars");
  }
  else{
      console.log("Check CTC currency && unit")
      resolve("Check CTC currency && unit");
  }
      },timetaken);
  })
  
  }

const upload = async (req, res) => {
  try {
    if (req.file == undefined) {
      return res.status(400).send("Please upload an excel file!");
    }

    let path =
      __basedir + "/fileUpload/files/" + req.file.filename;

    readXlsxFile(path).then((rows) => {
      // skip header
      rows.shift();

      let tutorials = [];

      rows.forEach((row) => {
        let tutorial = {
            name: row[0],
            Designation: row[1],
            company:row[2],
            experience: row[3],
            ctc_currency: row[4],
            ctc: row[5],
            ctc_type: row[6],
            email: row[7],
            phone_no: row[8],
            linkedIN: row[9],
            location: row[10],
        };

        tutorials.push(tutorial);
      });
     //  var final = db.log(tutorials);
     var data;
     (async()=>{
      data = await database(tutorials)   
      console.log(data)
      res.json(data);
   })();

    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: "Could not upload the file: " + req.file.originalname,
    });
  }
};



module.exports = {
  upload,
};
